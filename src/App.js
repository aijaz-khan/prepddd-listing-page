import React from 'react';
import logo from './logo.svg';
import './App.css';
import ListingPage from './components/ListingPage';

function App() {
  return <ListingPage />;
}

export default App;
