import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import InfoIcon from '@material-ui/icons/Info';
import DragIcon from '@material-ui/icons/DragIndicator';
import LinkIcon from '@material-ui/icons/Link';
import AddPersonIcon from '@material-ui/icons/PersonAdd';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutline';
import MoreIcon from '@material-ui/icons/MoreHoriz';
import { Divider, Button, Slide, withStyles } from '@material-ui/core';
import CustomizedTabs from '../Tabs';
const BlueCheckbox = withStyles({
  root: {
    // color: '#249AF2',
    '&$checked': {
      color: '#249AF2',
    },
  },
  checked: {},
})(props => <Checkbox color="default" {...props} />);
function createData(name, calories, fat, carbs, protein, img) {
  return { name, calories, fat, carbs, protein, img };
}

const rows = [
  createData(
    'Cupcake',
    'Just Link',
    37,
    67,
    4.3,
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSgCUUppDG3ItFTW_y08n2-GGSEe_uMiqCdFVavY1ZcoQwFcVlF'
  ),
  createData(
    'Donut',
    'Just Link',
    25.0,
    51,
    4.9,
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSgCUUppDG3ItFTW_y08n2-GGSEe_uMiqCdFVavY1ZcoQwFcVlF'
  ),
  createData(
    'Eclair',
    'Just Link Long data Sample',
    16.0,
    24,
    6.0,
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSgCUUppDG3ItFTW_y08n2-GGSEe_uMiqCdFVavY1ZcoQwFcVlF'
  ),
  createData(
    'Frozen yoghurt',
    'Just Link',
    6.0,
    24,
    4.0,
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSgCUUppDG3ItFTW_y08n2-GGSEe_uMiqCdFVavY1ZcoQwFcVlF'
  ),
  createData(
    'Gingerbread',
    'Just Link',
    100,
    49,
    3.9,
    'https://images.squarespace-cdn.com/content/5c47beb9da02bc7fba52d003/1559047132235-0MS28UXD5EILYFWP1LKT/Logo+FINAL+DARK+BLUE+-+no+BG_Logo_Only.png?content-type=image%2Fpng'
  ),
  createData(
    'Honeycomb',
    'Just Link',
    100,
    87,
    6.5,
    'https://images.squarespace-cdn.com/content/5c47beb9da02bc7fba52d003/1559047132235-0MS28UXD5EILYFWP1LKT/Logo+FINAL+DARK+BLUE+-+no+BG_Logo_Only.png?content-type=image%2Fpng'
  ),
  createData(
    'Ice cream sandwich',
    'Just Link',
    100,
    37,
    4.3,
    'https://images.squarespace-cdn.com/content/5c47beb9da02bc7fba52d003/1559047132235-0MS28UXD5EILYFWP1LKT/Logo+FINAL+DARK+BLUE+-+no+BG_Logo_Only.png?content-type=image%2Fpng'
  ),
  createData(
    'Jelly Bean',
    'Just Link',
    100,
    94,
    0.0,
    'https://images.squarespace-cdn.com/content/5c47beb9da02bc7fba52d003/1559047132235-0MS28UXD5EILYFWP1LKT/Logo+FINAL+DARK+BLUE+-+no+BG_Logo_Only.png?content-type=image%2Fpng'
  ),
  createData(
    'KitKat',
    'Just Link',
    26.0,
    65,
    7.0,
    'https://images.squarespace-cdn.com/content/5c47beb9da02bc7fba52d003/1559047132235-0MS28UXD5EILYFWP1LKT/Logo+FINAL+DARK+BLUE+-+no+BG_Logo_Only.png?content-type=image%2Fpng'
  )
];

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc'
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

const headRows = [
  {
    id: 'Company',
    numeric: false,
    disablePadding: true,
    label: 'Company'
  },
  { id: 'Name', numeric: true, disablePadding: false, label: 'Name' },
  { id: 'Status', numeric: true, disablePadding: false, label: 'Status' },
  { id: 'Modified', numeric: true, disablePadding: false, label: 'Modified' },
  { id: 'protein', numeric: true, disablePadding: false, label: '' }
];

function EnhancedTableHead(props) {
  const {
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort
  } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox" valign="middle" />
        <TableCell padding="checkbox">
          <BlueCheckbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'Select all desserts' }}
          />
        </TableCell>
        {headRows.map(row => (
          <TableCell
            key={row.id}
            align={'left'}
            padding={row.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === row.id ? order : false}
            style={numSelected === 1 ? { width: 120 } : {}}
          >
            <TableSortLabel
              active={orderBy === row.id}
              direction={order}
              onClick={createSortHandler(row.id)}
            >
              {row.label}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1)
  },
  align_center: {
    display: 'flex',
    alignItems: 'center'
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85)
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark
      },
  spacer: {
    flex: '1 1 100%'
  },
  actions: {
    color: theme.palette.text.secondary
  },
  title: {
    flex: '0 0 auto'
  }
}));

const EnhancedTableToolbar = props => {
  const classes = useToolbarStyles();
  const { numSelected } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subtitle1">
            {numSelected} selected
          </Typography>
        ) : (
            <Typography variant="h6" id="tableTitle">
              Lists
          </Typography>
          )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton aria-label="Delete">
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        ) : (
            <div style={{ display: 'flex' }}>
              <Tooltip title="Add a member">
                <IconButton aria-label="Filter list">
                  <AddPersonIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Link">
                <IconButton aria-label="Filter list">
                  <LinkIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Download">
                <IconButton aria-label="Filter list">
                  <CloudDownloadIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Remove">
                <IconButton aria-label="Filter list">
                  <DeleteOutlinedIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="More">
                <IconButton aria-label="Filter list">
                  <MoreIcon />
                </IconButton>
              </Tooltip>
            </div>
          )}
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    display: 'flex'
  },
  text_truncate: {
    maxWidth: 50,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap'
  },
  logo: { width: 20, height: 20, marginRight: 8 },
  align_center: { display: 'flex', alignItems: 'center' },
  btn_list: {
    position: 'absolute',
    top: 2,
    left: -45,
    color: '#249AF2',
    backgroundColor: 'white',
    fontWeight: 'bold',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 8,
    marginBottom: 8,
    width: 90,
    borderRadius: 0,
    borderWidth: 2,
    padding: 0,
    margin: 0,
    height: 30,
    display: 'flex',
    alignItems: 'center',
    borderColor: '#249AF2',
    '&:hover': {
      borderWidth: 2,
      borderColor: '#249AF2'
    }
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
    borderRight: '1px solid lightgrey'
  },
  table: {
    minWidth: 750
  },
  tableWrapper: {
    overflowX: 'auto'
  }
}));

export default function EnhancedTable() {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  function handleRequestSort(event, property) {
    const isDesc = orderBy === property && order === 'desc';
    setOrder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  }

  function handleSelectAllClick(event) {
    if (event.target.checked) {
      const newSelecteds = rows.map(n => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  }

  function handleClick(event, name) {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  }

  function handleChangePage(event, newPage) {
    setPage(newPage);
  }

  function handleChangeRowsPerPage(event) {
    setRowsPerPage(+event.target.value);
    setPage(0);
  }

  function handleChangeDense(event) {
    setDense(event.target.checked);
  }

  const isSelected = name => selected.indexOf(name) !== -1;

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  const [edit, setShowEdit] = useState(null);
  const [detail, setDetail] = useState(null);
  const selectedItem = rows.filter(row => row.name === selected[0]);
  return (
    <div className={classes.root}>
      <Paper className={classes.paper} square>
        <EnhancedTableToolbar numSelected={selected.length} />
        <Divider />
        <div className={classes.tableWrapper}>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
          >
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.name);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      onClick={event => handleClick(event, row.name)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.name}
                      selected={isItemSelected}
                      onMouseEnter={() => setShowEdit(row.name)}
                      onMouseLeave={() => setShowEdit(null)}
                    >
                      <TableCell padding="checkbox" valign="middle">
                        <DragIcon style={{ marginTop: 8 }} />
                      </TableCell>
                      <TableCell padding="checkbox">
                        <BlueCheckbox
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                        />
                      </TableCell>
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none"
                      >
                        <div className={classes.align_center}>
                          <img src={row.img} className={classes.logo} />
                          {row.name}
                        </div>
                      </TableCell>
                      <TableCell align="left" className={classes.text_truncate}>
                        {row.calories}
                      </TableCell>
                      <TableCell align="left">
                        <div className={classes.align_center}>
                          <div
                            style={{
                              backgroundColor:
                                row.fat === 100
                                  ? '#2AB94B'
                                  : edit === row.name
                                    ? '#249AF2'
                                    : '#1969A5',
                              width: selected.length === 1 ? 16 : 10,
                              height: 10,
                              borderRadius: 5,
                              marginRight: 8
                            }}
                          />
                          {row.fat !== 100
                            ? `${row.fat}/100 Complete`
                            : 'Complete'}
                        </div>
                      </TableCell>
                      <TableCell align="left">{row.carbs} Days ago</TableCell>
                      <TableCell align="left" style={{ padding: 0, position: 'relative' }}>
                        {row.name === edit ? (
                          <Button
                            variant="outlined"
                            className={classes.btn_list}
                            style={selected.length === 1 ? { left: 0, top:12 } : {}}
                          >
                            View List
                          </Button>
                        ) : (
                            ''
                          )}
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page'
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page'
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      <Paper elevation={0} square>
        {selected.length === 1 &&
          <Slide style={{ width: 350 }} in timeout={50} direction="right">
            <div>
              <div
                style={{
                  padding: 20,
                  display: 'flex',
                  justifyContent: 'space-between'
                }}
              >
                <div className={classes.align_center}>
                  <div>
                    <div
                      style={{
                        marginRight: 8,
                        fontWeight: 'bold',
                        fontSize: 25,
                        display: 'flex',
                        alignItems: 'center'
                      }}
                    >
                      Series B Funding{' '}
                      <img src={selectedItem[0].img} className={classes.logo} style={{ marginLeft: 6 }} />
                    </div>
                    <Typography variant="caption" color="textSecondary">
                      Requested by {selectedItem[0].calories} from{' '}
                      {selectedItem[0].name}{' '}
                    </Typography>
                  </div>
                </div>
                <IconButton onClick={() => setDetail(false)}>
                  <MoreIcon />
                </IconButton>
              </div>
              <CustomizedTabs />
            </div>
          </Slide>
        }</Paper>
    </div>
  );
}
